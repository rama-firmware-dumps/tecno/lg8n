#!/bin/bash

cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
cat vendor_boot.img.* 2>/dev/null >> vendor_boot.img
rm -f vendor_boot.img.* 2>/dev/null
cat system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null >> system_ext/apex/com.android.vndk.v30.apex
rm -f system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null
cat system_ext/app/AIVoiceAssistant/AIVoiceAssistant.apk.* 2>/dev/null >> system_ext/app/AIVoiceAssistant/AIVoiceAssistant.apk
rm -f system_ext/app/AIVoiceAssistant/AIVoiceAssistant.apk.* 2>/dev/null
cat system_ext/app/FaceID/FaceID.apk.* 2>/dev/null >> system_ext/app/FaceID/FaceID.apk
rm -f system_ext/app/FaceID/FaceID.apk.* 2>/dev/null
cat system_ext/app/ManualGuide/ManualGuide.apk.* 2>/dev/null >> system_ext/app/ManualGuide/ManualGuide.apk
rm -f system_ext/app/ManualGuide/ManualGuide.apk.* 2>/dev/null
cat system_ext/app/TranssionCamera/TranssionCamera.apk.* 2>/dev/null >> system_ext/app/TranssionCamera/TranssionCamera.apk
rm -f system_ext/app/TranssionCamera/TranssionCamera.apk.* 2>/dev/null
cat system_ext/priv-app/TranAlwaysOnDisplayV3/TranAlwaysOnDisplayV3.apk.* 2>/dev/null >> system_ext/priv-app/TranAlwaysOnDisplayV3/TranAlwaysOnDisplayV3.apk
rm -f system_ext/priv-app/TranAlwaysOnDisplayV3/TranAlwaysOnDisplayV3.apk.* 2>/dev/null
cat system_ext/priv-app/TranSettingsApk/TranSettingsApk.apk.* 2>/dev/null >> system_ext/priv-app/TranSettingsApk/TranSettingsApk.apk
rm -f system_ext/priv-app/TranSettingsApk/TranSettingsApk.apk.* 2>/dev/null
cat product/app/Gmail2/Gmail2.apk.* 2>/dev/null >> product/app/Gmail2/Gmail2.apk
rm -f product/app/Gmail2/Gmail2.apk.* 2>/dev/null
cat product/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null >> product/app/LatinImeGoogle/LatinImeGoogle.apk
rm -f product/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null
cat product/app/Maps/Maps.apk.* 2>/dev/null >> product/app/Maps/Maps.apk
rm -f product/app/Maps/Maps.apk.* 2>/dev/null
cat product/app/Photos/Photos.apk.* 2>/dev/null >> product/app/Photos/Photos.apk
rm -f product/app/Photos/Photos.apk.* 2>/dev/null
cat product/app/SpeechServicesByGoogle/SpeechServicesByGoogle.apk.* 2>/dev/null >> product/app/SpeechServicesByGoogle/SpeechServicesByGoogle.apk
rm -f product/app/SpeechServicesByGoogle/SpeechServicesByGoogle.apk.* 2>/dev/null
cat product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null >> product/app/TrichromeLibrary/TrichromeLibrary.apk
rm -f product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null
cat product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null >> product/app/WebViewGoogle/WebViewGoogle.apk
rm -f product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null
cat product/app/YouTube/YouTube.apk.* 2>/dev/null >> product/app/YouTube/YouTube.apk
rm -f product/app/YouTube/YouTube.apk.* 2>/dev/null
cat product/operator/app/FacebookStub/FacebookStub.apk.* 2>/dev/null >> product/operator/app/FacebookStub/FacebookStub.apk
rm -f product/operator/app/FacebookStub/FacebookStub.apk.* 2>/dev/null
cat product/operator/app/PalmPay/PalmPay.apk.* 2>/dev/null >> product/operator/app/PalmPay/PalmPay.apk
rm -f product/operator/app/PalmPay/PalmPay.apk.* 2>/dev/null
cat product/operator/app/Snapchat/Snapchat.apk.* 2>/dev/null >> product/operator/app/Snapchat/Snapchat.apk
rm -f product/operator/app/Snapchat/Snapchat.apk.* 2>/dev/null
cat product/operator/app/TikTok/TikTok.apk.* 2>/dev/null >> product/operator/app/TikTok/TikTok.apk
rm -f product/operator/app/TikTok/TikTok.apk.* 2>/dev/null
cat product/operator/app/TikTokzhiliao/TikTokzhiliao.apk.* 2>/dev/null >> product/operator/app/TikTokzhiliao/TikTokzhiliao.apk
rm -f product/operator/app/TikTokzhiliao/TikTokzhiliao.apk.* 2>/dev/null
cat product/operator/app/WPSOffice/WPSOffice.apk.* 2>/dev/null >> product/operator/app/WPSOffice/WPSOffice.apk
rm -f product/operator/app/WPSOffice/WPSOffice.apk.* 2>/dev/null
cat product/operator/app/WPSOffice/oat/arm64/WPSOffice.vdex.* 2>/dev/null >> product/operator/app/WPSOffice/oat/arm64/WPSOffice.vdex
rm -f product/operator/app/WPSOffice/oat/arm64/WPSOffice.vdex.* 2>/dev/null
cat product/priv-app/ARCore/ARCore.apk.* 2>/dev/null >> product/priv-app/ARCore/ARCore.apk
rm -f product/priv-app/ARCore/ARCore.apk.* 2>/dev/null
cat product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> product/priv-app/GmsCore/GmsCore.apk
rm -f product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat product/priv-app/Messages/Messages.apk.* 2>/dev/null >> product/priv-app/Messages/Messages.apk
rm -f product/priv-app/Messages/Messages.apk.* 2>/dev/null
cat product/priv-app/Velvet/Velvet.apk.* 2>/dev/null >> product/priv-app/Velvet/Velvet.apk
rm -f product/priv-app/Velvet/Velvet.apk.* 2>/dev/null
